<?php

namespace Drupal\example_a\Plugin\EntityAccessCondition;

use Drupal\Core\Session\AccountInterface;
use Drupal\entity_access_policies\Condition\Value\EntityCondition;
use Drupal\entity_access_policies\Condition\Value\EntityConditionGroup;
use Drupal\entity_access_policies\EntityAccessCondition\EntityAccessFieldConditionBase;

/**
 * Whether the entity is stickied.
 *
 * @EntityAccessFieldCondition(
 *   id = "sticky",
 *   required_interfaces = {
 *     "\Drupal\node\NodeInterface"
 *   }
 * )
 */
class StickyCondition extends EntityAccessFieldConditionBase {

  /**
   * {@inheritdoc}
   */
  public function getEntityCondition(AccountInterface $account) {
    return new EntityCondition('uid.0.entity.uid.0.value', 1, '=');
    //return new EntityCondition('sticky.0.value', (integer) $this->configuration['stickied'], '=');
  }

}
