<?php

namespace Drupal\example_a\Plugin\EntityAccessCondition;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\entity_access_policies\EntityAccessCondition\EntityAccessPostConditionBase;

/**
 * Whether the node title exceeds the configured length.
 *
 * @EntityAccessPostCondition(
 *   id = "title_contains",
 *   required_interfaces = {
 *     "\Drupal\node\NodeInterface",
 *   }
 * )
 */
class TitleContainsCondition extends EntityAccessPostConditionBase {

  /**
   * {@inheritdoc}
   */
  public function evaluate(AccountInterface $account, EntityInterface $entity) {
    $title = $entity->getTitle();
    return strpos($title, $this->configuration['string']) !== FALSE;
  }

}
