<?php

namespace Drupal\example_a\Plugin\EntityAccessCondition;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\entity_access_policies\EntityAccessCondition\EntityAccessPostConditionBase;

/**
 * Whether the node title exceeds the configured length.
 *
 * @EntityAccessPostCondition(
 *   id = "title_length",
 *   required_interfaces = {
 *     "\Drupal\node\NodeInterface"
 *   }
 * )
 */
class TitleLengthCondition extends EntityAccessPostConditionBase {

  /**
   * {@inheritdoc}
   */
  public function evaluate(AccountInterface $account, EntityInterface $entity) {
    $title = $entity->getTitle();
    $title_length = strlen($title);
    return $title_length > (integer) $this->configuration['length'];
  }

}
