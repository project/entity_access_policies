<?php

namespace Drupal\entity_access_policies\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an EntityAccessConditionType type annotation object.
 *
 * Entity access condition plugins use an object-based annotation method, rather
 * than an array-type annotation method (as commonly used on other annotation
 * types). The annotation properties of entity access conditions are found on
 * \Drupal\entity_access_policies\EntityAccessConditionType\EntityAccessConditionType
 * and are accessed using get/set methods defined in
 * \Drupal\entity_access_policies\EntityAccessConditionType\EntityAccessConditionTypeInterface.
 *
 * @Annotation
 */
class EntityAccessCondition extends Plugin {

  /**
   * The class used to represent the entity access condition.
   *
   * It must implement \Drupal\entity_access_policies\EntityAccessConditionType\EntityAccessConditionTypeInterface.
   *
   * @var string
   */
  public $entity_access_condition_class = 'Drupal\entity_access_policies\EntityAccessCondition\EntityAccessConditionType';

  /**
   * {@inheritdoc}
   */
  public function get() {
    $values = $this->definition;

    // Use the specified entity access condition class, and remove it before instantiating.
    $class = $values['entity_access_condition_class'];
    unset($values['entity_access_condition_class']);

    return new $class($values);
  }

}
