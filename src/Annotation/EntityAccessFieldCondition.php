<?php

namespace Drupal\entity_access_policies\Annotation;;

/**
 * Defines an entity access field condition annotation object.
 *
 * The annotation properties of entity access conditions are found on
 * \Drupal\entity_access_policies\EntityAccessConditionType\EntityAccessFieldConditionType
 * and are accessed using get/set methods defined in
 * \Drupal\entity_access_policies\EntityAccessConditionType\EntityAccessFieldConditionTypeInterface.
 *
 * @Annotation
 */
class EntityAccessFieldCondition extends EntityAccessCondition {

  /**
   * {@inheritdoc}
   */
  public $entity_access_condition_class = 'Drupal\entity_access_policies\EntityAccessCondition\EntityAccessFieldConditionType';

}
