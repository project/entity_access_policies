<?php

namespace Drupal\entity_access_policies\Annotation;;

/**
 * Defines an entity access postcondition annotation object.
 *
 * The annotation properties of entity access conditions are found on
 * \Drupal\entity_access_policies\EntityAccessConditionType\EntityAccessPostConditionType
 * and are accessed using get/set methods defined in
 * \Drupal\entity_access_policies\EntityAccessConditionType\EntityAccessPostConditionTypeInterface.
 *
 * @Annotation
 */
class EntityAccessPostCondition extends EntityAccessCondition {

  /**
   * {@inheritdoc}
   */
  public $entity_access_condition_class = 'Drupal\entity_access_policies\EntityAccessCondition\EntityAccessPostConditionType';

}
