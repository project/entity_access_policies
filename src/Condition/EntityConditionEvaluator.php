<?php

namespace Drupal\entity_access_policies\Condition;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Plugin\DataType\EntityAdapter;
use Drupal\Core\TypedData\DataReferenceInterface;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\entity_access_policies\Condition\Value\EntityCondition;
use Drupal\entity_access_policies\Condition\Value\EntityConditionGroup;

class EntityConditionEvaluator {

  /**
   * Evaluates the given conditions against the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity against which to evaluate the conditions
   * @param \Drupal\entity_access_policies\Condition\Value\EntityCondition|\Drupal\entity_access_policies\Condition\Value\EntityConditionGroup
   *   The condition or condition group to evaluate against the entity.
   */
  public static function evaluate(EntityInterface $entity, $condition) {
    if ($condition instanceof EntityCondition) {
      $cache_metadata = CacheableMetadata::createFromObject($entity);
      $value = static::extract($entity->getTypedData(), explode('.', $condition->field()), $cache_metadata);
      $result = static::compare($value, $condition->value(), $condition->operator());
      return AccessResult::allowedIf($result)->addCacheableDependency($cache_metadata);
    }
    elseif ($condition instanceof EntityConditionGroup) {
      foreach ($condition->members() as $member) {
        $this_result = static::evaluate($entity, $member);
        if (!isset($result)) {
          $result = $this_result;
        }
        elseif ($condition->conjunction() === 'AND') {
          $result = $result->andIf($this_result);
          if (!$result->isAllowed()) {
            return $result;
          }
        }
        elseif ($result) {
          $result = $result->orIf($this_result);
          if (!$result->isAllowed()) {
            return $result;
          }
        }
      }
    }
    else {
      throw new \InvalidArgumentException('You must pass either an EntityCondition or EntityConditionGroup.');
    }
  }

  /**
   * Naively compare values.
   */
  protected static function compare($a, $b, $operator) {
    switch ($operator) {
      case '=':
        return $a == $b;
      case '<>':
        return $a != $b;
      default:
        return FALSE;
    }
  }

  /**
   * Naively extract a value from a typed data object.
   */
  protected static function extract(TypedDataInterface $data, $path, RefinableCacheableDependencyInterface $cache_metadata) {
    $value = $data->get(reset($path));

    if ($value instanceof DataReferenceInterface && current($path) === 'entity') {
      $target = $value->getTarget();
      if ($target instanceof EntityAdapter) {
        $cache_metadata->addCacheableDependency($target->getValue());
      }
      return static::extract($target, array_slice($path, 1), $cache_metadata);
    }

    if (count($path) > 1) {
      return static::extract($value, array_slice($path, 1), $cache_metadata);
    }

    return $value->getValue();
  }

}
