<?php

namespace Drupal\entity_access_policies\Condition\Value;

class EntityConditionGroup {

  /**
   * The AND conjunction value.
   */
  protected static $allowedConjunctions = ['AND', 'OR'];

  /**
   * The conjunction.
   *
   * @var string
   */
  protected $conjunction;

  /**
   * The members of the condition group.
   *
   * A list of EntityConditions and/or EntityConditionGroups.
   *
   * @var array
   */
  protected $members;

  /**
   * Constructs a new condition group object.
   *
   * @param string $conjunction
   *   The group conjunction to use.
   * @param array $members
   *   (optional) The group conjunction to use.
   */
  public function __construct($conjunction, $members = []) {
    if (!in_array($conjunction, self::$allowedConjunctions)) {
      throw new \InvalidArgumentException('Allowed conjunctions: AND, OR.');
    }
    $this->conjunction = $conjunction;
    $this->members = $members;
  }

  /**
   * The condition group conjunction.
   *
   * @return string
   */
  public function conjunction() {
    return $this->conjunction;
  }

  /**
   * The members which belong to the the condition group.
   *
   * A list of EntityConditions and/or EntityConditionGroups.
   *
   * @return array
   */
  public function members() {
    return $this->members;
  }

}
