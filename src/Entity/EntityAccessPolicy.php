<?php

namespace Drupal\entity_access_policies\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\entity_access_policies\EntityAccessPolicyInterface;

/**
 * @ConfigEntityType(
 *   id = "entity_access_policy",
 *   config_prefix = "policy",
 *   label = @Translation("Entity Access Policy"),
 *   admin_permission = "administer entity access policies",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "entity_type_ids",
 *     "operations",
 *     "enabled",
 *     "priority",
 *     "conditions",
 *   },
 * )
 */
class EntityAccessPolicy extends ConfigEntityBase implements EntityAccessPolicyInterface {

  /**
   * The policy id.
   *
   * @var string
   */
  public $id;

  /**
   * The policy label.
   *
   * @var string
   */
  public $label;

  /**
   * Whether the policy should be actively applied.
   *
   * @var boolean
   */
  protected $enabled;

  /**
   * The priority of the policy.
   *
   * Policies with a higher priority are evaluated first. Policies which are
   * considered fast and which would deny access to a great number of entities
   * should receive higher priorities. Conversely, policies which are only
   * applicable in a small number of scenarios or which are time-consuming
   * should have a low priority. The default priority is 0.
   *
   * @var integer
   */
  protected $priority;

  /**
   * The conditions to evaluate against the entity under access control.
   *
   * @var \Drupal\Core\Condition\ConditionInterface[]
   */
  protected $conditions;

  /**
   * {@inheritdoc}
   */
  public function applies(EntityInterface $entity, $operation) {
    // TODO evaluate applicability of the policy to the given entity and operation.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getApplicableEntityTypeIds() {
    return $this->entity_type_ids;
  }

  /**
   * {@inheritdoc}
   */
  public function getApplicableOperations() {
    return $this->operations;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled() {
    return $this->enabled;
  }

  /**
   * {@inheritdoc}
   */
  public function getPriority() {
    return $this->priority;
  }

  /**
   * {@inheritdoc}
   */
  public function getConditions() {
    return $this->conditions;
  }

}
