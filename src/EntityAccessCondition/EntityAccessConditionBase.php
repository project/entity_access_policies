<?php

namespace Drupal\entity_access_policies\EntityAccessCondition;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Cache\RefinableCacheableDependencyTrait;

/**
 * Defines a base entity access condition class.
 */
abstract class EntityAccessConditionBase extends PluginBase implements EntityAccessConditionInterface {

  use RefinableCacheableDependencyTrait;

  /**
   * {@inheritdoc}
   */
  public function isCompatibleWithEntityType($entity_type_id) {
    return $this->getPluginDefinition()->isCompatibleWithEntityType($entity_type_id);
  }

}
