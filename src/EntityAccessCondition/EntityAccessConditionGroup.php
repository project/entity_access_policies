<?php

namespace Drupal\entity_access_policies\EntityAccessCondition;

use Drupal\Core\Cache\RefinableCacheableDependencyTrait;

/**
 * @internal
 */
class EntityAccessConditionGroup {

  use RefinableCacheableDependencyTrait;

  /**
   * The conjunction of the condition group.
   *
   * @var string
   */
  protected $conjunction;

  /**
   * The members of this condition group.
   *
   * @see \Drupal\entity_access_policies\EntityAccessCondition\EntityAccessConditionGroup
   * @see \Drupal\entity_access_policies\EntityAccessCondition\EntityAccessConditionInterface
   *
   * @var mixed
   */
  protected $members;

  /**
   * Constructs a new entity access condition group.
   *
   * @see \Drupal\entity_access_policies\EntityAccessCondition\EntityAccessConditionGroup
   * @see \Drupal\entity_access_policies\EntityAccessCondition\EntityAccessConditionInterface
   *
   * @param string $conjuction
   *   Either AND or OR.
   * @param mixed $members
   *   The subgroups or conditions for this group. These must be either
   *   EntityAccessConditionGroups or EntityAccessConditions.
   */
  public function __construct($conjunction, $members) {
    assert('Drupal\\Component\\Assertion\\Inspector::assertAllRegularExpressionMatch("/(AND|OR)/", [$conjunction])');
    assert(
      'Drupal\\Component\\Assertion\\Inspector::assertAllObjects(
        $members,
        \'\\Drupal\\entity_access_policies\\EntityAccessCondition\\EntityAccessConditionGroup\',
        \'\\Drupal\\entity_access_policies\\EntityAccessCondition\\EntityAccessConditionInterface\'
      )'
    );

    $this->conjunction = $conjunction;
    $this->members = $members;

    // Incorporate the child member cache deps into this groups cache deps.
    foreach ($this->members as $member) {
      $this->addCacheableDependency($member);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getConjunction() {
    return $this->conjunction;
  }

  /**
   * {@inheritdoc}
   */
  public function getMembers() {
    return $this->members;
  }

  public function hasFieldConditions() {
    foreach ($this->members as $member) {
      if ($member instanceof EntityAccessConditionGroup && $member->hasFieldConditions()) {
        return TRUE;
      }
      if ($member instanceof EntityAccessFieldConditionInterface) {
        return TRUE;
      }
    }
    return FALSE;
  }

  public function hasPostConditions() {
    foreach ($this->members as $member) {
      if ($member instanceof EntityAccessConditionGroup && $member->hasPostConditions()) {
        return TRUE;
      }
      if ($member instanceof EntityAccessPostConditionInterface) {
        return TRUE;
      }
    }
    return FALSE;
  }

}
