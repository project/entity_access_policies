<?php

namespace Drupal\entity_access_policies\EntityAccessCondition;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Query\ConditionInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\entity_access_policies\Condition\EntityConditionEvaluator;

/**
 * A stateful condition group evaluator.
 *
 * EntityConditionGroups are value objects, they cannot evaluate themselves.
 * Since we need to evaluate these groups in different phases, this object
 * provides a way to store state between phase evaluations and encapsulates the
 * conjunction logic as it evaluates the group members.
 */
class EntityAccessConditionGroupEvaluator {

  const INITIAL_STATE = 0;
  const PRE_PHASE_COMPLETE = 1;
  const FIELD_PHASE_COMPLETE = 2;
  const POST_PHASE_COMPLETE = 3;

  /**
   * The current phase of evaluation.
   */
  protected $phase;

  /**
   * The condition group members under evaluation.
   *
   * @see \Drupal\entity_access_policies\EntityAccessCondition\EntityAccessConditionGroup
   * @see \Drupal\entity_access_policies\EntityAccessCondition\EntityAccessConditionInterface
   *
   * @var mixed
   */
  protected $members;

  /**
   * The currently evaluated access result
   *
   * @var \Drupal\Core\Access\AccessResultInterface|NULL
   */
  protected $currentAccessResult = NULL;

  /**
   * Whether there are field conditions to be evaluated.
   *
   * @var boolean
   */
  protected $hasFieldConditions;

  /**
   * Whether there are post conditions to be evaluated.
   *
   * @var boolean
   */
  protected $hasPostConditions;

  /**
   * A new condition evaluator.
   *
   * @param \Drupal\entity_access_policies\EntityAccessCondition\EntityAccessConditionGroup $condition_group
   *   A condition group to be evaluated.
   */
  public function __construct(EntityAccessConditionGroup $condition_group) {
    $this->phase = static::INITIAL_STATE;
    $this->members = array_map(function ($member) {
      if ($member instanceof EntityAccessConditionGroup) {
        return new static($member);
      }
      return $member;
    }, $condition_group->getMembers());
    $this->hasFieldConditions = $condition_group->hasFieldConditions();
    $this->hasPostConditions = $condition_group->hasPostConditions();
    $this->conjunction = $condition_group->getConjunction();
  }

  /**
   * Whether the evaluation can be considered complete.
   *
   * @return boolean
   *   Whether the evaluation is complete.
   */
  public function isFinished() {
    // If we already have a valid, allowed result we don't care if there are
    // unevaluated conditions.
    if ($this->conjunction === 'OR' && ($this->currentAccessResult && $this->currentAccessResult->isAllowed())) {
      return TRUE;
    }

    switch ($this->phase) {
      case static::INITIAL_STATE:
        return FALSE;
      case static::PRE_PHASE_COMPLETE:
        return !$this->hasFieldConditions && !$this->hasPostConditions;
      case static::FIELD_PHASE_COMPLETE:
        return !$this->hasPostConditions;
      case static::POST_PHASE_COMPLETE:
        return TRUE;
    }
  }

  /**
   * The access result.
   *
   * Must be called after evaluation has been completed.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The composite evaluated access result.
   *
   * @throws \LogicException
   *   Thrown if the method is called before evaluation is complete.
   */
  public function getAccessResult() {
    if (!$this->isFinished()) {
      throw new \LogicException('Cannot get access result before evaluation is complete.');
    }
    return (!is_null($this->currentAccessResult)) ? $this->currentAccessResult : AccessResult::neutral();
  }

  /**
   * Constructs a query condition which limits known, inaccessible entities.
   */
  public function getQueryCondition(ConditionInterface $condition) {
    if ($this->phase != static::PRE_PHASE_COMPLETE) {
      throw new \LogicException('getQueryCondition must be called after evaluatePreConditions.');
    }

    if ($this->conjunction === 'AND') {
      $group = $condition->andConditionGroup();
    }
    else {
      $group = $condition->orConditionGroup();
    }
    
    // TODO: evaluate and add any field conditions which would prohibit access
    // to an entity, i.e. which, unless TRUE would cause the whole condition
    // set to be FALSE. For now, the group is just emtpy, providing no
    // real optimization.
    //
    // Any field conditions in an AND group can be added.
    //
    // Field conditions in an OR group MAY be added if their are no post
    // conditions, there are ONLY field conditions. If there are only
    // preconditions and field conditions, we can add the field conditions
    // when EVERY precondition has been evaluated and was FALSE.

    return $group;
  }

  /**
   * Evaluate all preconditions in the condition set.
   *
   * @return $this
   */
  public function evaluatePreConditions(AccountInterface $account) {
    if ($this->phase != static::INITIAL_STATE) {
      throw new \LogicException('evaluatePreConditions must be called before any other evaluation.');
    }

    $this->evaluate(function ($member) use ($account) {
      if ($member instanceof EntityAccessPreConditionInterface) {
        return AccessResult::allowedIf($member->evaluate($account));
      }
      elseif ($member instanceof EntityAccessConditionGroupEvaluator) {
        if ($member->evaluatePreConditions($account)->isFinished()) {
          return $member->getAccessResult();
        }
      }
    });

    $this->phase = static::PRE_PHASE_COMPLETE;

    return $this;
  }

  /**
   * Evaluate all field conditions in the condition set.
   *
   * @return $this
   */
  public function evaluateFieldConditions(AccountInterface $account, EntityInterface $entity) {
    if ($this->phase != static::PRE_PHASE_COMPLETE) {
      throw new \LogicException('evaluateFieldConditions must only be called after evaluatePreConditions.');
    }

    $this->evaluate(function ($member) use ($account, $entity) {
      if ($member instanceof EntityAccessFieldConditionInterface) {
        return EntityConditionEvaluator::evaluate($entity, $member->getEntityCondition($account));
      }
      elseif ($member instanceof EntityAccessConditionGroupEvaluator) {
        if ($member->evaluateFieldConditions($account, $entity)->isFinished()) {
          return $member->getAccessResult();
        }
      }
    });

    $this->phase = static::FIELD_PHASE_COMPLETE;

    return $this;
  }

  /**
   * Evaluate all post conditions in the condition set.
   *
   * @return $this
   */
  public function evaluatePostConditions(AccountInterface $account, EntityInterface $entity) {
    if ($this->phase != static::FIELD_PHASE_COMPLETE) {
      throw new \LogicException('evaluatePostConditions must only be called after evaluateFieldConditions.');
    }

    $this->evaluate(function ($member) use ($account, $entity) {
      if ($member instanceof EntityAccessPostConditionInterface) {
        return AccessResult::allowedIf($member->evaluate($account, $entity));
      }
      elseif ($member instanceof EntityAccessConditionGroupEvaluator) {
        if ($member->evaluatePostConditions($account, $entity)->isFinished()) {
          return $member->getAccessResult();
        }
      }
    });

    $this->phase = static::POST_PHASE_COMPLETE;

    return $this;
  }

  /**
   * Lazily evaluates conditions with respect for the group conjunction.
   *
   * This method optimizes the evaluation of a entity conditions.
   *
   * When the conjunction is AND, this method will stop evaluating conditions as
   * soon we have a result which is not explicity "allowed" and simply always
   * return neutral.
   *
   * When the conjunction is OR, this method will stop evaluating conditions as
   * soon we have an "allowed" result.
   *
   * A NULL result will be interpreted as neutral() by higher level code.
   *
   * @param callable $evaluator
   *   A callable which selectively evaluates the condition group members. The
   *   evaluator should only evaluate members relevant to the given phase of
   *   execution. The callable should return an AccessResultInterface or NULL
   *   when the given member was not evaluated.
   *
   * @return Drupal\Core\Access\AccessResultInterface|NULL
   *   An access result with cache metadata or NULL, if no members were
   *   evaluated and no initial result existed.
   */
  protected function evaluate($evaluator) {
    if ($this->phase !== static::INITIAL_STATE) {
      $access_result = $this->currentAccessResult;
    }

    foreach ($this->members as $member) {
      if (!isset($access_result)) {
        $access_result = $evaluator($member);
      }
      elseif ($this->conjunction === 'AND' && $access_result->isAllowed()) {
        if ($this_result = $evaluator($member)) {
          $access_result = $access_result->andIf($this_result);
          // Once a member has been evaluated, we add its cache metadata to
          // our access result. We don't add groups.
          if (!($member instanceof EntityAccessConditionGroup)) {
            $access_result->addCacheableDependency($member);
          }
        }
      }
      elseif ($this->conjunction === 'OR' && !$access_result->isAllowed()) {
        if ($this_result = $evaluator($member)) {
          $access_result = $access_result->orIf($this_result);
          // Once a member has been evaluated, we add its cache metadata to
          // our access result. We don't add groups.
          if (!($member instanceof EntityAccessConditionGroup)) {
            $access_result->addCacheableDependency($member);
          }
        }
      }
    }

    $this->currentAccessResult = (isset($access_result)) ? $access_result : NULL;
  }

}
