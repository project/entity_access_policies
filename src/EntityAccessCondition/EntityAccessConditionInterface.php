<?php

namespace Drupal\entity_access_policies\EntityAccessCondition;

use Drupal\Component\Plugin\DerivativeInspectionInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines a common interface for all entity access conditions.
 */
interface EntityAccessConditionInterface extends PluginInspectionInterface, DerivativeInspectionInterface{

  /**
   * @todo Remove? See implementation calling the type.
   *
   * Checks whether an entity access condition supports a given entity type.
   *
   * @param string $entity_type_id
   *   The ID of the the entity type to check compatibility with.
   *
   * @return boolean
   *   Whether the plugin supports the provided entity type.
   */
  public function isCompatibleWithEntityType($entity_type_id);

}
