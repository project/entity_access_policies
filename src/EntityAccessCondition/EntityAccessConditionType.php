<?php

namespace Drupal\entity_access_policies\EntityAccessCondition;

use Drupal\Component\Plugin\Definition\PluginDefinition;

/**
 * Provides an implementation of an entity access condition type.
 */
class EntityAccessConditionType extends PluginDefinition implements EntityAccessConditionTypeInterface {

  /**
   * A list of required interfaces.
   *
   * @var string[]
   *
   * @see EntityAccessConditionTypeInterface::getRequiredInterfaces()
   */
  protected $required_interfaces = [];

  /**
   * Any additional properties and values.
   *
   * @var array
   */
  protected $additional = [];

  /**
   * Constructs a new EntityAccessConditionType.
   *
   * @param array $definition
   *   An array of values from the annotation.
   */
  public function __construct($definition) {
    foreach ($definition as $property => $value) {
      $this->set($property, $value);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function get($property) {
    if (property_exists($this, $property)) {
      $value = isset($this->{$property}) ? $this->{$property} : NULL;
    }
    else {
      $value = isset($this->additional[$property]) ? $this->additional[$property] : NULL;
    }
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function set($property, $value) {
    if (property_exists($this, $property)) {
      $this->{$property} = $value;
    }
    else {
      $this->additional[$property] = $value;
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRequiredInterfaces() {
    return $this->required_interfaces;
  }

  /**
   * {@inheritdoc}
   */
  public function isCompatibleWithEntityType($entity_type_id) {
    $entity_type = \Drupal::EntityTypeManager()->getDefinition($entity_type_id);

    foreach ($this->getRequiredInterfaces() as $interface) {
      if (!$entity_type->entityClassImplements($interface)) {
        return FALSE;
      }
    }

    return TRUE;
  }

}
