<?php

namespace Drupal\entity_access_policies\EntityAccessCondition;

use Drupal\Component\Plugin\Definition\PluginDefinitionInterface;

/**
 * Defines a common interface for all entity access condition types.
 */
interface EntityAccessConditionTypeInterface extends PluginDefinitionInterface {

  /**
   * Gets any arbitrary property.
   *
   * @param string $property
   *   The property to retrieve.
   *
   * @return mixed
   *   The value for that property, or NULL if the property does not exist.
   */
  public function get($property);

  /**
   * Sets a value to an arbitrary property.
   *
   * @param string $property
   *   The property to use for the value.
   * @param mixed $value
   *   The value to set.
   *
   * @return $this
   */
  public function set($property, $value);

  /**
   * Retrieves a list of interfaces subject entity types must implement.
   *
   * Access conditions need a way to specify what the entity they will receive
   * has to be capable of in order for the condition to be able to check for
   * access. This is possible by specifying a list of interfaces in the
   * annotation that the entity type must implement. An empty list means there
   * are no specific requirements and so all entity types are valid.
   *
   * @return string[]
   *   A list of fully qualified class names for the required interfaces.
   */
  public function getRequiredInterfaces();

  /**
   * Checks whether an entity access condition type supports a given entity type.
   *
   * @param string $entity_type_id
   *   The ID of the the entity type to check compatibility with.
   *
   * @return boolean
   *   Whether the entity access condition type supports the provided entity type.
   */
  public function isCompatibleWithEntityType($entity_type_id);

}
