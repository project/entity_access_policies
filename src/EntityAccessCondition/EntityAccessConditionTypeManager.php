<?php

namespace Drupal\entity_access_policies\EntityAccessCondition;

use Drupal\Component\Assertion\Inspector;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Plugin\Discovery\AnnotatedClassDiscovery;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Manages entity access condition type plugin definitions.
 *
 * Each entity access condition definition array is set in the entity access
 * condition's annotation and altered by hook_entity_access_condition_alter().
 *
 * @see \Drupal\entity_access_policies\Annotation\EntityAccessCondition
 * @see \Drupal\entity_access_policies\EntityAccessCondition\EntityAccessConditionType
 * @see \Drupal\entity_access_policies\EntityAccessCondition\EntityAccessConditionTypeInterface
 */
class EntityAccessConditionTypeManager extends DefaultPluginManager implements EntityAccessConditionTypeManagerInterface, ContainerAwareInterface {

  use ContainerAwareTrait;

  /**
   * Constructs a new entity access condition plugin manager.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations,
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend to use.
   */
  public function __construct(\Traversable $namespaces, ModuleHandlerInterface $module_handler, CacheBackendInterface $cache) {
    parent::__construct(
      'Plugin/EntityAccessCondition',
      $namespaces,
      $module_handler,
      'Drupal\entity_access_policies\EntityAccessCondition\EntityAccessConditionInterface'
    );

    $this->setCacheBackend($cache, 'entity_access_condition', ['entity_access_conditions']);
    $this->alterInfo('entity_access_condition');

    $this->discovery = new AnnotatedClassDiscovery(
      'Plugin/EntityAccessCondition',
      $namespaces,
      'Drupal\entity_access_policies\Annotation\EntityAccessCondition'
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getInstance(array $options) {
    if (!$this->validOptions($options)) {
      throw new InvalidConfigurationException('Invalid entity access condition configuration.');
    }

    // TODO: Should 'group' be a plugin type or is it fine to hardcode?
    if ($options['type'] === 'group') {
      $members = array_map(function ($member) {
        return $this->getInstance($member);
      }, $options['members']);
      return new EntityAccessConditionGroup($options['conjunction'], $members);
    }

    return $this->createInstance($options['type'], $options);
  }

  /**
   * {@inheritdoc}
   */
  public function processDefinition(&$definition, $plugin_id) {
    /** @var EntityAccessConditionTypeInterface $definition */
    parent::processDefinition($definition, $plugin_id);

    // All required interfaces must exist.
    foreach ($definition->getRequiredInterfaces() as $interface) {
      if (!interface_exists($interface)) {
        throw new InterfaceNotFoundException("Interface '$interface' for entity access condition type '$plugin_id' could not be found.");
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinition($plugin_id, $exception_on_invalid = TRUE) {
    if (($entity_access_condition = parent::getDefinition($plugin_id, FALSE)) && class_exists($entity_access_condition->getClass())) {
      return $entity_access_condition;
    }
    elseif (!$exception_on_invalid) {
      return NULL;
    }

    throw new PluginNotFoundException($plugin_id, sprintf('The "%s" entity access condition type does not exist.', $plugin_id));
  }

  protected function validOptions($options) {
    return ((
        array_key_exists('type', $options) && $options['type'] === 'group' &&
        array_key_exists('conjunction', $options) && in_array($options['conjunction'], ['AND', 'OR']) &&
        array_key_exists('members', $options)
      ) || (
        array_key_exists('type', $options) && $this->hasDefinition($options['type'])
        // TODO: It would be nice allow plugins to validate their own config.
      )
    );
  }

}
