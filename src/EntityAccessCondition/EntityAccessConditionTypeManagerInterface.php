<?php

namespace Drupal\entity_access_policies\EntityAccessCondition;

use Drupal\Component\Plugin\Discovery\CachedDiscoveryInterface;
use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Provides an interface for entity access condition managers.
 */
interface EntityAccessConditionTypeManagerInterface extends PluginManagerInterface, CachedDiscoveryInterface {

}
