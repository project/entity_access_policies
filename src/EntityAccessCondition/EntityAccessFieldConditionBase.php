<?php

namespace Drupal\entity_access_policies\EntityAccessCondition;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Implements field condition specific enhancements to the EntityAccessConditionBase class.
 */
abstract class EntityAccessFieldConditionBase extends EntityAccessConditionBase implements EntityAccessFieldConditionInterface {

}
