<?php

namespace Drupal\entity_access_policies\EntityAccessCondition;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines a common interface for all entity access field condition objects.
 */
interface EntityAccessFieldConditionInterface extends EntityAccessConditionInterface {

  /**
   * Provides a set of entity field conditions with which to evaluate access.
   *
   * This method regulates access by checking the returned set of conditions
   * against field values available on the entity under access control. The
   * entity conditions may be used to construct a limited entity query or can
   * be evaluated at runtime.
   *
   * The method may return a single condition or a condition group. A condition
   * group allows multiple conditions to be combined with a conjuction and
   * can nest other groups to create complex rules.
   *
   * Example:
   *   The following would be true for every account which IS NOT (<>) the owner
   *   of the entity under access control.
   *
   *   return new EntityCondition('uid', $account->id(), '<>');
   *
   * @see \Drupal\entity_access_policies\Condition\Value\EntityCondition
   * @see \Drupal\entity_access_policies\Condition\Value\EntityConditionGroup
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account requiring access.
   *
   * @return \Drupal\entity_access_policies\Condition\Value\EntityCondition|\Drupal\entity_access_policies\Condition\Value\EntityConditionGroup
   *   The conditions to evaluate.
   */
  public function getEntityCondition(AccountInterface $account);

}
