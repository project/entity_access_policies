<?php

namespace Drupal\entity_access_policies\EntityAccessCondition;

/**
 * Provides an implementation of an entity access field condition type.
 */
class EntityAccessFieldConditionType extends EntityAccessConditionType implements EntityAccessFieldConditionTypeInterface {

}
