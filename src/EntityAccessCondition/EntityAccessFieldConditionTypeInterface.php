<?php

namespace Drupal\entity_access_policies\EntityAccessCondition;

/**
 * Provides an interface for an entity access field condition type.
 */
interface EntityAccessFieldConditionTypeInterface extends EntityAccessConditionTypeInterface {

}
