<?php

namespace Drupal\entity_access_policies\EntityAccessCondition;

/**
 * Implements postcondition specific enhancements to the EntityAccessConditionBase class.
 */
abstract class EntityAccessPostConditionBase extends EntityAccessConditionBase implements EntityAccessPostConditionInterface {

}
