<?php

namespace Drupal\entity_access_policies\EntityAccessCondition;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines a common interface for all entity access postcondition objects.
 */
interface EntityAccessPostConditionInterface extends EntityAccessConditionInterface {

  /**
   * Determines access for a given account and entity.
   *
   * This method should not be considered extremely performant. It is here only
   * to determine access over those entities that were not filtered out by pre-
   * conditions or field conditions. The better the performance, the faster the
   * system will be able to determine access, though.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account requiring access.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity under access control.
   *
   * @return boolean
   *   The result of the condition evaluation.
   */
  public function evaluate(AccountInterface $account, EntityInterface $entity);

}
