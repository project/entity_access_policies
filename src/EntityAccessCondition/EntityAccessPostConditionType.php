<?php

namespace Drupal\entity_access_policies\EntityAccessCondition;

/**
 * Provides an implementation of an entity access postcondition type.
 */
class EntityAccessPostConditionType extends EntityAccessConditionType implements EntityAccessPostConditionTypeInterface {

}
