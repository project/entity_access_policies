<?php

namespace Drupal\entity_access_policies\EntityAccessCondition;

/**
 * Provides an interface for an entity access postcondition type.
 */
interface EntityAccessPostConditionTypeInterface extends EntityAccessConditionTypeInterface {

}
