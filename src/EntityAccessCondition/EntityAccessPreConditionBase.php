<?php

namespace Drupal\entity_access_policies\EntityAccessCondition;

/**
 * Implements precondition specific enhancements to the EntityAccessConditionBase class.
 */
abstract class EntityAccessPreConditionBase extends EntityAccessConditionBase implements EntityAccessPreConditionInterface {

}
