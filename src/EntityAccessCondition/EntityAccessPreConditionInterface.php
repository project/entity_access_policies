<?php

namespace Drupal\entity_access_policies\EntityAccessCondition;

use Drupal\Core\Session\AccountInterface;

/**
 * Defines a common interface for all entity access precondition objects.
 */
interface EntityAccessPreConditionInterface extends EntityAccessConditionInterface {

  /**
   * Determines access solely from the account or environment.
   *
   * This method should derive its result from the given account or the
   * environment. Where possible, it should only be used when the check is
   * considered performant (i.e. loading information from the database is
   * discouraged here).
   *
   * The entity under access control is intentionally not available.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account requiring access.
   *
   * @return boolean
   *   The result of the evaluation.
   */
  public function evaluate(AccountInterface $account);

}
