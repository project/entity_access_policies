<?php

namespace Drupal\entity_access_policies\EntityAccessCondition;

/**
 * Provides an implementation of an entity access precondition type.
 */
class EntityAccessPreConditionType extends EntityAccessConditionType implements EntityAccessPreConditionTypeInterface {

}
