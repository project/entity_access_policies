<?php

namespace Drupal\entity_access_policies\EntityAccessCondition;

/**
 * Provides an interface for an entity access precondition type.
 */
interface EntityAccessPreConditionTypeInterface extends EntityAccessConditionTypeInterface {

}
