<?php

namespace Drupal\entity_access_policies\EntityAccessCondition;

use Drupal\Component\Plugin\Exception\PluginException;

/**
 * Indicates that an interface could not be found.
 */
class InterfaceNotFoundException extends PluginException {

}
