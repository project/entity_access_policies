<?php

namespace Drupal\entity_access_policies\EntityAccessCondition;

use Drupal\Component\Plugin\Exception\PluginException;

/**
 * Indicates that the configuration for an entity access condition is invalid.
 */
class InvalidConfigurationException extends PluginException {

}
