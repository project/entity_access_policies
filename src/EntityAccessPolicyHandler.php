<?php

namespace Drupal\entity_access_policies;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\QueryInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\entity_access_policies\EntityAccessCondition\EntityAccessConditionGroupEvaluator;
use Drupal\entity_access_policies\EntityAccessCondition\EntityAccessConditionTypeManagerInterface;

class EntityAccessPolicyHandler implements EntityAccessPolicyHandlerInterface {

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The entity access condition type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $conditionManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The config entity storage interface.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $configStorage;

  /**
   * Constructs a new entity access policy manager.
   */
  public function __construct(AccountInterface $current_user, EntityAccessConditionTypeManagerInterface $condition_manager, EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory) {
    $this->currentUser = $current_user;
    $this->conditionManager = $condition_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->configStorage = $entity_type_manager->getStorage('entity_access_policy');
  }

  /**
   * {@inheritdoc}
   */
  public function access(EntityInterface $entity, $operation, AccountInterface $account = NULL) {
    $account = ($account) ? $account : $this->currentUser;
    $condition_set = $this->loadConditionSet($entity->getEntityTypeId(), $operation);
    $access_result = AccessResult::neutral();

    // When no policy sets exist, we have nothing to say.
    if (empty($condition_set->getMembers())) {
      return $access_result;
    }

    // Make sure to cache access if our conditions change or the context on
    // which they depend changes.
    $access_result->addCacheableDependency($condition_set);

    // Initialize a new evaluator for every condition group.
    $evaluator = new EntityAccessConditionGroupEvaluator($condition_set);

    // TODO: This should be able go to a static cache for the phase 1 result.

    // Phase 1: evaluate preconditions.
    if ($evaluator->evaluatePreConditions($account)->isFinished()) {
      if ($evaluator->getAccessResult()->isAllowed()) {
        return $evaluator->getAccessResult();
      }
    }

    // TODO: Skip evaluation of field conditions which were used to limit the
    // query which fetched this entity?

    // Phase 2: evaluate field conditions.
    if ($evaluator->evaluateFieldConditions($account, $entity)->isFinished()) {
      if ($evaluator->getAccessResult()->isAllowed()) {
        return $evaluator->getAccessResult();
      }
    }

    // TODO: Phase 3 should be persisted to cache.

    // Phase 3: evaluate post conditions.
    if ($evaluator->evaluatePostConditions($account, $entity)->isFinished()) {
      if ($evaluator->getAccessResult()->isAllowed()) {
        return $evaluator->getAccessResult();
      }
    }

    // We've evaluated everything and nothing was allowed, so deny access.
    $forbidden = AccessResult::forbidden('Access forbidden by the entity access policy system.');
    return $evaluator->getAccessResult()->andIf($forbidden);
  }

  /**
   * {@inheritdoc}
   */
  public function limitQueryByAccess(QueryInterface $query, AccountInterface $account = NULL) {
    $account = ($account) ? $account : $this->currentUser;
    $entity_type_id = $query->getEntityTypeId();
    $condition_set = $this->loadConditionSet($entity_type_id, 'view');

    if ($condition_set->evaluatePreConditions($account)->isFinished()) {
      // We already know the account can view any entity of the type because the
      // condition was completely evaluated. Either the user can view any entity
      // or none of them.
      if ($condition_set->getAccessResult()->isAllowed()) {
        return;
      }
      else {
        // Try to "poison" the query by setting a condition that can't be true.
        // @TODO Explore a more reliable method of doing this.
        $entity_type = $this->entityTypeManager->getDefinition($query->getEntityTypeId());
        if ($entity_type) {
          $query->notExists($entity_type->getKey('id'));
          return;
        }
      }
    }

    // There must be addtional conditions which affect access to the queried
    // entities. We can attemp to limit the query using any field conditions.
    // It is *NOT* guaranteed that all entities are accessible. A final access
    // check *MUST* be evaluated for each entity at runtime.
    $query->condition($condition_set->getQueryCondition($query));
  }

  /**
   * Loads all policy condition groups, sorted by policy priority.
   *
   * @param string $entity_type_id
   *   The entity type for which to retrieve policies.
   * @param string $operation
   *   The operation for which to retrieve policies.
   *
   * @return Drupal\entity_access_policies\EntityAccessCondition\EntityAccessConditionGroup[]
   *   The priority sorted entity access condition groups.
   */
  protected function loadConditionSet($entity_type_id, $operation) {
    // Get all policies for the given type and operation, sorted by priority.
    $policies = $this->sortPolicies($this->getPolicies($entity_type_id, $operation));

    // Bundle policies of the same priority.
    $groups = array_reduce($policies, function ($groups, $policy) {
      $groups[$policy->getPriority()][] = $policy;
      return $groups;
    }, []);

    // Constructs a meta condition of all policies.
    return $this->conditionManager->getInstance([
      'type' => 'group',
      'conjunction' => 'AND', // All groups are conjoined with AND.
      'members' => array_map(function ($policies) {
        return [
          'type' => 'group',
          'conjunction' => 'OR', // Policies within a group and conjoined with OR.
          'members' => array_map(function ($policy) {
            // Top-level conditions of a policy are conjoined with AND.
            return [
              'type' => 'group',
              'conjunction' => 'AND',
              'members' => $policy->getConditions()
            ];
          }, $policies),
        ];
      }, $groups),
    ]);
  }

  /**
   * Gets the policies applicable to the given entity type and operation.
   *
   * @param string $entity_type_id
   *   The entity type for which to retrieve policies.
   * @param string $operation
   *   The operation for which to retrieve policies.
   *
   * @return \Drupal\entity_access_policies\EntityAccessPolicyInterface[]
   *   The applicable entity access policies.
   */
  protected function getPolicies($entity_type_id, $operation) {
    $policies = $this->configStorage->loadMultiple();
    return array_filter($policies, function ($policy) use ($entity_type_id, $operation) {
      return (
        in_array($entity_type_id, $policy->getApplicableEntityTypeIds()) &&
        in_array($operation, $policy->getApplicableOperations())
      );
    });
  }

  /**
   * Sorts policies by their priority.
   *
   * @param Drupal\entity_access_policies\EntityAccessPolicyInterface[] $policies
   *   The policies to sort.
   *
   * @return Drupal\entity_access_policies\EntityAccessPolicyInterface[]
   */
  protected function sortPolicies(array $policies) {
    usort($policies, function ($a, $b) {
      $ap = $a->getPriority();
      $bp = $b->getPriority();
      if ($ap == $bp) return 0;
      return ($ap > $bp) ? -1 : 1;
    });
    return $policies;
  }

}
