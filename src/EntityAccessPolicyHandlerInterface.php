<?php

namespace Drupal\entity_access_policies;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\QueryInterface;
use Drupal\Core\Session\AccountInterface;

interface EntityAccessPolicyHandlerInterface {

  /**
   * Evaluate all applicable policies to determine access for the given account.
   *
   * @param Drupal\Core\Entity\EntityInterface $entity
   *   The entity for which to evaluate access.
   * @param string $operation
   *   The operation for which to evaluate access.
   * @param Drupal\Core\Session\AccountInterface $account
   *   (optional) The user for which to check access, or NULL to check access
   *   for the current user. Defaults to NULL.
   */
  public function access(EntityInterface $entity, $operation, AccountInterface $account = NULL);

  /**
   * Limits the given entity query by excluding impossible to access entities.
   *
   * It is *NOT GUARANTEED* that all entities are accessible. A final access
   * check *MUST* be evaluated for each entity at runtime.
   *
   * Many policies depend solely on the account, operation and values of the
   * entity under access control, i.e. they do not rely on information which
   * can only be understoof after the entity is retrieved. In those scenarios,
   * it is efficient to limit the number of entities retrieved before they are
   * loaded.
   *
   * In some instances, an account may simply be unable to access any entity.
   * In this case, the query is "poisoned" with an unsatisfiable condition so
   * that no entities are returned.
   *
   * @param Drupal\Core\Entity\QueryInterface $query
   *   The query to limit by access.
   * @param Drupal\Core\Entity\AccountInterface $account
   *   (optional) The user for which to check access, or NULL to check access
   *   for the current user. Defaults to NULL.
   */
  public function limitQueryByAccess(QueryInterface $query, AccountInterface $account = NULL);

}
