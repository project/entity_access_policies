<?php

namespace Drupal\entity_access_policies;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityInterface;

interface EntityAccessPolicyInterface extends ConfigEntityInterface {

  /**
   * The entity type ids to which this policy applies.
   *
   * @return string[]
   *   The entity type identifiers to which this policy applies.
   */
  public function getApplicableEntityTypeIds();

  /**
   * The operations to which this policy applies.
   *
   * @return string[]
   *   The operations to which this policy applies.
   */
  public function getApplicableOperations();

  /**
   * Whether the policy is actively being applied to entities.
   *
   * @return boolean
   */
  public function isEnabled();

  /**
   * The priority of the access policy.
   *
   * @return integer
   */
  public function getPriority();

  /**
   * The condition configuration of the policy.
   *
   * @return array
   *   The entity access configuration.
   */
  public function getConditions();

}
