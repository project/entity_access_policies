<?php

namespace Drupal\example_a\Plugin\EntityAccessCondition;

use Drupal\Core\Session\AccountInterface;
use Drupal\entity_access_policies\EntityAccessCondition\EntityAccessPreConditionBase;

/**
 * Whether the account has the given permission.
 *
 * @EntityAccessFieldCondition(
 *   id = "permission",
 * )
 */
class PermissionCondition extends EntityAccessPreConditionBase {

  /**
   * {@inheritdoc}
   */
  public function evaluate(AccountInterface $account) {
    $this->addCacheableDependency($account);
    return $account->hasPermission($this->configuration['permission']);
  }

}
