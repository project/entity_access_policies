<?php

namespace Drupal\entity_access_policies\Plugin\EntityAccessCondition;

use Drupal\Core\Session\AccountInterface;
use Drupal\entity_access_policies\Condition\Value\EntityCondition;
use Drupal\entity_access_policies\EntityAccessCondition\EntityAccessFieldConditionBase;

/**
 * Whether the entity is published.
 *
 * @EntityAccessFieldCondition(
 *   id = "status",
 *   required_interfaces = {
 *     "\Drupal\Core\Entity\EntityPublishedInterface"
 *   }
 * )
 */
class StatusCondition extends EntityAccessFieldConditionBase {

  /**
   * {@inheritdoc}
   */
  public function getEntityCondition(AccountInterface $account) {
    return new EntityCondition('status.0.value', (integer) $this->configuration['published'], '=');
  }

}
